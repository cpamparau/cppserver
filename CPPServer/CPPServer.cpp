#include <iostream>
#include <winsock2.h>
#include <thread>
#include <functional>
#include <vector>
#include "conio.h"

using namespace std;

typedef std::shared_ptr<std::thread> thread_ptr;
SOCKET server;

SOCKADDR_IN serverAddr, clientAddr;
int client_id=0;
void client_handler_thread(SOCKET client);
#pragma comment(lib, "Ws2_32.lib")
int main()
{
    WSADATA WSAData;

    std::vector<thread_ptr> client_threads;

    WSAStartup(MAKEWORD(2, 0), &WSAData);
    server = socket(AF_INET, SOCK_STREAM, 0);

    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(5555);

    bind(server, (SOCKADDR*)&serverAddr, sizeof(serverAddr));
    listen(server, 0);

    cout << "Listening for incoming connections..." << endl;
    while(true) {
        int clientAddrSize = sizeof(clientAddr);
        SOCKET client = accept(server, (SOCKADDR*)&clientAddr, &clientAddrSize);
        if (client != INVALID_SOCKET)
        {
            client_threads.push_back(make_shared<std::thread>(std::thread(std::bind(&client_handler_thread, client))));
        }
    } ;
}

void client_handler_thread(SOCKET client)
{
    char buffer[1024];
    cout << "Client" << client_id++ << " connected!" << endl;
    int octeti = -1;
    do
    {
        octeti = recv(client, buffer, sizeof(buffer), 0);
        cout << "Client says: " << buffer << "[ " << octeti << " octeti]" << endl;
        memset(buffer, 0, sizeof(buffer));
    } while (octeti != -1);


    closesocket(client);
    cout << "Client " << client_id-1 << " disconnected." << endl;
}